import { Client } from "discord.js";
import { getRepository } from "typeorm";
import { PokeRequest } from "../entity/PokeRequest";
import { appLogger } from "../util/logger";
import pickRandom from "../util/pickRandom";


const pokeStrings = [
    "Hewwo %s, there's new message~",
    "Henlo %s, pinging you just because.",
    "Hi %s ",
    "👉 %s",
    "%s",
    "%s (>‿◠)✌",
    "%s ٩(˘◡˘)۶",
    "%s (•◡•) /",
    "%s （っ＾▿＾）",
    "%s ☜(ˆ▿ˆc)",
    "(👍≖‿‿≖)👍 %s 👍(≖‿‿≖👍)",
];

export default async function runPoker(client: Client): Promise<void> {
    const pokeRepo = getRepository(PokeRequest);

    appLogger.info("CRON: Poker has been sumonned");
    let toPoke = await pokeRepo.find({ where: { hasNewMessage: true, deletedOn: null } });
    await Promise.all(toPoke.map(async (pokeRequest) => {
        appLogger.info(`CRON: Summoning ${pokeRequest.channelId}@${pokeRequest.guildId}`);
        let targetChannel = client.channels.cache.get(pokeRequest.channelId);
        if (!targetChannel?.isText()) {
            appLogger.info(`CRON: ${pokeRequest.channelId}@${pokeRequest.guildId} Channel is not text :(`)
            return;
        }
        await targetChannel.send(pickRandom(pokeStrings, { count: 1 })[0].replace("%s", pokeRequest.getDiscordMentionablesString()));
        pokeRequest.hasNewMessage = false;
        return await pokeRepo.save(pokeRequest);
    }))
    return Promise.resolve();
    appLogger.info("CRON: Job done!")
}