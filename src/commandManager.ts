import { Routes } from "discord-api-types/v9";
import { CommandInteraction } from "discord.js";
import { getRepository } from "typeorm";
import { Allow } from "./commands/allow";
import { BaseCommand } from "./commands/baseCommand";
import { Poke } from "./commands/poke";
import { CommandRegistry } from "./entity/CommandRegistry";
import { GuildRegistry } from "./entity/GuildRegistry";
import BaseExceptionAsResponse from "./exceptions/baseExceptionAsResponse";
import { getDiscordRestClient } from "./util/discordRestClient";
import errorToEmbed from "./util/errorToEmbed";
import { appLogger } from "./util/logger";

export type Commandlist = Record<string, { new(): BaseCommand }>;

export class CommandManager {

    protected static basicCommands: Commandlist = {
        "allow": Allow
    }

    protected static guildCommands: Commandlist = {
        "poke": Poke
    }

    protected static allCommands: Commandlist = { ...this.basicCommands, ...this.guildCommands };

    public static async registerInitialCommand(guild: GuildRegistry) {
        appLogger.info(`[${guild.guildId}] Trying to install command for this guild`)
        await this.installCommands(guild, this.basicCommands);
        appLogger.info(`[${guild.guildId}] Slash command installed.`)
    }


    public static async installFullCommand(guild: GuildRegistry) {
        appLogger.info(`[${guild.guildId}] Trying to install full command to this guild`)
        await this.installCommands(guild, this.allCommands);
        appLogger.info(`[${guild.guildId}] Full slash command installed.`)
    }

    private static async installCommands(guild: GuildRegistry, commands: Commandlist): Promise<CommandRegistry[]> {
        const commandRepo = getRepository(CommandRegistry);

        appLogger.info(`[${guild.guildId}] Installing all commands...`)
        let cmds = Object.keys(commands).map(element => {
            let theClass = new commands[element]();
            return theClass.register();
        })
        let installedIds: any = await getDiscordRestClient()
            .put(Routes.applicationGuildCommands(process.env.DISCORD_APPID || "", guild.guildId), { body: cmds });

        let installedCommands = Object.keys(commands).map(async (element, idx) => {

            let theClass = new commands[element]();
            let theCommand = await commandRepo.findOne({ where: { commandName: element, server: guild.id } });
            if (!theCommand) {
                theCommand = new CommandRegistry();
                theCommand.commandName = element;
                theCommand.server = guild;
            }
            theCommand.version = theClass.version;
            theCommand.commandId = installedIds[idx]?.id;

            commandRepo.save(theCommand);
            appLogger.info(`[${guild.guildId}] Command ${element} registered and installed.`)
            return theCommand;
        });

        return await Promise.all(installedCommands);
    }

    public static async refreshCommand(guild: GuildRegistry) {
        appLogger.info(`[${guild.guildId}] Refreshing commands...`)
        await this.installCommands(guild, this.allCommands);
        appLogger.info(`[${guild.guildId}] Command refreshed.`)
    }

    public static async removeBasicCommand(guild: GuildRegistry) {
        //uninstall non-basic commands
        appLogger.info(`[${guild.guildId}] Removing commands...`)
        let commandRepo = getRepository(CommandRegistry);
        let dcClient = getDiscordRestClient();
        await Promise.all(Object.keys(this.guildCommands).map(async command => {
            let theCommand = await commandRepo.findOne({ where: { commandName: command, server: guild.id } });
            if (theCommand) {
                try {
                    await dcClient.delete(Routes.applicationGuildCommand(process.env.DISCORD_APPID || "", guild.guildId, theCommand.commandId));
                    await commandRepo.delete(theCommand);
                    appLogger.info(`[${guild.guildId}] Command removed: ${command}`)
                } catch (e) {
                    appLogger.warn(`[${guild.guildId}] Error while removing ${command} on ${theCommand.commandId} (CommandRegistry#${theCommand.id}).`);
                }
            }
        }))
        appLogger.info(`[${guild.guildId}] Slashcommands deleted.`)
    }

    public static async runCommand(interaction: CommandInteraction): Promise<void> {
        appLogger.info(`[${interaction.guildId}] command runner initiated`)

        try {
            let theCommand = interaction.commandName;
            if (!this.allCommands[theCommand]) {
                // this one is quite fatal!
                throw Error(`Invalid command ${theCommand}`);
            }

            let initializedRunner = new this.allCommands[theCommand]();
            await initializedRunner.do(interaction);
        } catch (e) {
            if (e instanceof BaseExceptionAsResponse) {
                if (!interaction.replied) {
                    interaction.reply({ embeds: [e.toEmbedMessage()] });
                } else {
                    interaction.followUp({ embeds: [e.toEmbedMessage()] });
                }
            } else if (e instanceof Error || e instanceof ReferenceError) {
                appLogger.error("Fatal error:", e);
                interaction.reply({ content: "Fatal error occured, please contact admin.", embeds: [errorToEmbed(e)] });
            } else {
                appLogger.error("Fatal UNKOWN error:", e);
                interaction.reply({ content: "Fatal, unknown error." });
            }
        }
        appLogger.info(`[${interaction.guildId}] command runner finished.`)
    }
}