import { Column, CreateDateColumn, DeleteDateColumn, Entity, Index, ManyToOne, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";
import { GuildRegistry } from "./GuildRegistry";

@Entity()
@Index(["commandId", "server"], { unique: true })
export class CommandRegistry {
    @PrimaryGeneratedColumn()
    id: number;

    @ManyToOne(type => GuildRegistry, guild => guild.commands)
    server: GuildRegistry;

    @Column()
    commandId: string;

    @Column()
    commandName: string;

    @Column()
    version: number;

    @CreateDateColumn()
    createdOn: Date;

    @UpdateDateColumn()
    updatedOn: Date;

    @DeleteDateColumn()
    deletedOn: Date;
}
