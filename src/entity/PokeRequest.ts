import { roleMention, userMention } from "@discordjs/builders";
import { Column, CreateDateColumn, DeleteDateColumn, Entity, Index, PrimaryGeneratedColumn } from "typeorm";


export enum PingToType {
    USER = 1,
    ROLE = 2
}

@Entity()
@Index(["guildId", "channelId", "pingTo", "pingType"], { unique: true })
export class PokeRequest {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    guildId: string;

    @Column()
    channelId: string;

    @Column()
    pingTo: string;

    @Column({
        type: "enum",
        enum: PingToType
    })
    pingType: PingToType;

    @Column({
        default: false
    })
    hasNewMessage: boolean = false;

    @Column()
    addedBy: string;

    @CreateDateColumn()
    createdOn: Date;

    @DeleteDateColumn()
    deletedOn: Date;

    public getDiscordMentionablesString(): string {
        let mentionee = userMention(this.pingTo);
        if (this.pingType == PingToType.ROLE) {
            mentionee = roleMention(this.pingTo);
        }

        return mentionee;
    }
}
