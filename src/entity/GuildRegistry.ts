import { Column, CreateDateColumn, DeleteDateColumn, Entity, ManyToOne, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";
import { CommandRegistry } from "./CommandRegistry";

@Entity()
export class GuildRegistry {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ unique: true })
    guildId: string;

    @Column({ default: false })
    isAllowed: boolean = false;

    @Column({ nullable: true })
    allowedBy: string;

    @ManyToOne(type => CommandRegistry, commands => commands.server)
    commands: CommandRegistry[];

    @CreateDateColumn()
    createdOn: Date;

    @UpdateDateColumn()
    updatedOn: Date;

    @DeleteDateColumn()
    deletedOn: Date;

}
