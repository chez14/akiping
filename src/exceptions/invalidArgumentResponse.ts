import BaseExceptionAsResponse from "./baseExceptionAsResponse";

export default class InvalidArgumentResponse extends BaseExceptionAsResponse {
    public embedTitle = "Invalid Input"
}