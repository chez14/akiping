import { ColorResolvable, MessageEmbed } from "discord.js";

export default class BaseExceptionAsResponse extends Error {
    public embedColor: ColorResolvable = "#EA7600";
    public embedTitle = "Base Exception";

    public embedHelper: string | null = null;

    public constructor(message: string, additionalHelper: string | null = null) {
        super(message);
        this.embedHelper = additionalHelper;
    }

    public toEmbedMessage(): MessageEmbed {
        let embeder = new MessageEmbed();
        embeder.setColor(this.embedColor)
            .setTitle(this.embedTitle)
            .setDescription(this.message)

        if (this.embedHelper) {
            embeder.addField("More info:", this.embedHelper);
        }

        return embeder;
    }
}