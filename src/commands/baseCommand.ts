import { SlashCommandBuilder } from "@discordjs/builders";
import { CommandInteraction } from "discord.js";

export abstract class BaseCommand {

    public version: number = 1;

    public abstract register(): SlashCommandBuilder;

    public abstract do(interaction: CommandInteraction): Promise<void>;

}