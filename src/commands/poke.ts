import { SlashCommandBuilder, time, userMention } from "@discordjs/builders";
import { APIInteractionDataResolvedGuildMember, APIRole } from "discord-api-types/v9";
import { CacheType, CommandInteraction, Guild, GuildMember, MessageEmbed, Permissions, Role, TextBasedChannels, User } from "discord.js";
import { getRepository, QueryFailedError } from "typeorm";
import { PingToType, PokeRequest } from "../entity/PokeRequest";
import InvalidArgumentResponse from "../exceptions/invalidArgumentResponse";
import { BaseCommand } from "./baseCommand";

export class Poke extends BaseCommand {
    public version = 1;

    public register(): SlashCommandBuilder {
        const theCommand = new SlashCommandBuilder();
        theCommand.setName("poke")
            .setDescription("Poke someone when certain channel got new message~!")
            .addSubcommand(sub => sub
                .setName("add")
                .setDescription("Mention this person when new message on certain channel recieved.")
                .addMentionableOption(ipt => ipt
                    .setName("user-or-role")
                    .setDescription("Someone/role to mention to")
                    .setRequired(true)
                )
            ).addSubcommand(sub => sub
                .setName("remove")
                .setDescription("Remove all mention on this channel")
                .addMentionableOption(ipt => ipt
                    .setName("user-or-role")
                    .setDescription("Someone/role to mention to")
                    .setRequired(true)
                )

            ).addSubcommand(sub => sub
                .setName("list")
                .setDescription("Lists all installed mentionables on this channel")
            )

        return theCommand;
    }

    public async do(interaction: CommandInteraction<CacheType>): Promise<void> {
        if (
            interaction.user.id != process.env.DISCORD_HEATHCLIFF &&
            !(<Permissions>interaction.member.permissions).has(Permissions.FLAGS.MANAGE_MESSAGES)
        ) {
            throw new InvalidArgumentResponse(
                "Sorry, you don't have permission to manage messages in this channel."
            )
        }

        if (!interaction.channel || !interaction.guild) {
            throw new InvalidArgumentResponse("This command needs to be ran in server.", "Try to execute this on server, maybe that will help, idk.")
        }

        if (interaction.options.getSubcommand() == "list") {
            return await this.list(interaction.channel, interaction.guild, interaction)
        }

        let mentionables = this.mentionablesSolver(interaction.options.getMentionable('user-or-role', true));
        if (!mentionables) {
            throw new InvalidArgumentResponse("You didn't provide someone to mention.", "Try to add someone to mention, maybe that will help, idk.")
        }

        if (interaction.options.getSubcommand() == "add") {
            await this.add(mentionables, interaction.channel, interaction.guild, <GuildMember>interaction.member);
            interaction.reply("r👻, poke added!")
        } else if (interaction.options.getSubcommand() == "remove") {
            await this.remove(mentionables, interaction.channel, interaction.guild);
            interaction.reply("r👻, poke removed.")
        }
    }

    protected mentionablesSolver(role: User | Role | GuildMember | APIInteractionDataResolvedGuildMember | APIRole): GuildMember | Role | null {
        if (role instanceof GuildMember || role instanceof Role) {
            return role;
        }
        return null;
    }


    protected async add(mentionables: GuildMember | Role, channel: TextBasedChannels, guild: Guild, requester: GuildMember): Promise<void> {
        let pokeRepo = getRepository(PokeRequest);
        let theRequest = new PokeRequest();
        theRequest.pingTo = mentionables.id;
        theRequest.pingType = (mentionables instanceof GuildMember ? PingToType.USER : PingToType.ROLE)
        theRequest.guildId = guild.id;
        theRequest.channelId = channel.id;
        theRequest.addedBy = requester.id;
        try {
            await pokeRepo.save(theRequest);
        } catch (e) {
            if (e instanceof QueryFailedError && e.message.startsWith('ER_DUP_ENTRY')) {
                return Promise.resolve();
            }
            throw e;
        }
    }

    protected async remove(mentionables: GuildMember | Role, channel: TextBasedChannels, guild: Guild): Promise<void> {
        let pokeRepo = getRepository(PokeRequest);
        let thePoke = await pokeRepo.findOne({
            where: {
                pingTo: mentionables.id,
                pingType: (mentionables instanceof GuildMember ? PingToType.USER : PingToType.ROLE),
                channelId: channel.id,
                guildId: guild.id
            }
        });
        if (!thePoke) {
            throw new InvalidArgumentResponse("Hyeh, i don't remember such poke reminder.");
        }
        await pokeRepo.remove(thePoke);
    }

    protected async list(channel: TextBasedChannels, guild: Guild, interaction: CommandInteraction): Promise<void> {
        let pokeRepo = getRepository(PokeRequest);
        let thePoke = await pokeRepo.find({
            where: {
                channelId: channel.id,
                guildId: guild.id
            },
            order: {
                "id": "ASC"
            }
        });

        let theTable = thePoke
            .map(poke =>
                `${poke.getDiscordMentionablesString()}, added on ${time(poke.createdOn)} by ${userMention(poke.addedBy)}`
            ).join("\n");


        let dataTable = new MessageEmbed();
        dataTable.setTitle("Poke Lists")
            .setDescription(theTable);

        await interaction.reply({ content: "Here's the list:", embeds: [dataTable] });
    }
}