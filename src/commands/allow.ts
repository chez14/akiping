import { codeBlock, SlashCommandBuilder } from "@discordjs/builders";
import { CacheType, CommandInteraction, MessageEmbed } from "discord.js";
import { getRepository } from "typeorm";
import { CommandManager } from "../commandManager";
import { GuildRegistry } from "../entity/GuildRegistry";
import { BaseCommand } from "./baseCommand";

export class Allow extends BaseCommand {

    public version = 1;

    public register(): SlashCommandBuilder {
        const theCommand = new SlashCommandBuilder();
        theCommand.setName("allow")
            .setDescription("Allows/disallow akiping to run on this server. This command only works for owner.")
            .addSubcommand(subcommand => subcommand.setName("approve").setDescription("Allow this server to use this bot"))
            .addSubcommand(subcommand => subcommand.setName("revoke").setDescription("Revoke permission this server to use this bot"));

        return theCommand;
    }

    public async do(interaction: CommandInteraction<CacheType>): Promise<void> {
        if (interaction.user.id != process.env.DISCORD_HEATHCLIFF) {
            interaction.reply("You're not the owner of this bot. Ask the owner to add your server to allowlist.");
            return Promise.resolve();
        }

        let guildRepo = getRepository(GuildRegistry);
        let guildRegistry = await guildRepo.findOne({ where: { guildId: interaction.guildId } });
        if (!guildRegistry) {
            throw new Error("Guild hasn't been registered before.");
        }

        if (interaction.options.getSubcommand() == "approve") {
            guildRegistry.allowedBy = interaction.user.id;
            guildRegistry.isAllowed = true;

            await guildRepo.save(guildRegistry);

            await CommandManager.installFullCommand(guildRegistry);

            await interaction.reply("Server has been added to allowlist. More info will be sent via ephemeral message.");

            const embedded = new MessageEmbed();
            embedded.setTitle("Server Info")
                .addField("`guildId`", interaction.guildId)
                .addField("`guildRegistryId`", `${guildRegistry.id}`)
                .addField("Full Dump", codeBlock("js", JSON.stringify(guildRegistry)))
                .setColor("#0077c8");
            await interaction.followUp({
                ephemeral: true,
                content: "As promised, here's your follow up info:",
                embeds: [embedded]
            })
        } else {
            guildRegistry.allowedBy = interaction.user.id;
            guildRegistry.isAllowed = false;

            await CommandManager.removeBasicCommand(guildRegistry);

            await guildRepo.save(guildRegistry);

            await interaction.reply("Server has been removed from allowlist.");
        }
    }
}