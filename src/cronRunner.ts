import { Client } from "discord.js";
import { schedule } from "node-cron";
import runPoker from "./cron/poker";

export function initCronjob(discord: Client) {
    schedule("0 0 0 */2 * *", () => {
        // trigger the poker
        runPoker(discord);
    })
}