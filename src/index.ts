import { Client, Intents } from "discord.js";
import getUrls from "get-urls";
import "reflect-metadata";
import { CLIENT_RENEG_LIMIT } from "tls";
import { createConnection, getConnection, getRepository } from "typeorm";
import { CommandManager } from "./commandManager";
import { initCronjob } from "./cronRunner";
import { GuildRegistry } from "./entity/GuildRegistry";
import { PokeRequest } from "./entity/PokeRequest";
import { appLogger } from "./util/logger";
require('dotenv').config()


appLogger.info("App starting...")
createConnection().then(async connection => {
    appLogger.info("Database connected.")

    const client = new Client({
        intents: [
            Intents.FLAGS.GUILDS,
            Intents.FLAGS.GUILD_MESSAGES,
            Intents.FLAGS.DIRECT_MESSAGES
        ], partials: [
            'CHANNEL', // Required to receive DMs
            'MESSAGE', // Required to receive DMs
        ]
    });
    client.on('ready', () => {
        appLogger.info(`Logged in as ${client?.user?.tag}!`)

        client.user?.setActivity({
            type: "PLAYING",
            name: `${process.env.CI_COMMIT_TAG || 'latest'} (${process.env.CI_COMMIT_SHA})`
        })
        //trigger run the crojob.
        initCronjob(client);
    });

    // bind client to guild message
    const pokeReq = getRepository(PokeRequest);
    client.on('messageCreate', async (message) => {
        if (message.author.bot || message.partial) {
            return;
        }
        if (!message.guildId || !message.channelId) {
            await message.reply("I don't support private message. Sorry~")
            return; // not in guild or channel.
        }

        // trigger update only when see links
        let theUrls = getUrls(message.content, {
            requireSchemeOrWww: true,
            exclude: [
                "^https\:\/\/cdn.discordapp.com\/",
                "^https\:\/\/tenor.com\/"
            ]
        });
        
        if (Array.from(theUrls).length < 1) {
            return;
        }

        await getConnection()
            .createQueryBuilder()
            .update(PokeRequest)
            .set({ hasNewMessage: true })
            .where({ guildId: message.guildId, channelId: message.channelId })
            .execute();
    })

    const guildReg = getRepository(GuildRegistry);
    client.on('guildCreate', async (guild) => {
        appLogger.info("Guild invite " + guild.id);
        let theGuild = await guildReg.findOne({ where: { guildId: guild.id } });

        if (!theGuild) {
            theGuild = new GuildRegistry();
            theGuild.guildId = guild.id;
            theGuild.isAllowed = false;
            await guildReg.save(theGuild);
        }
        await CommandManager.registerInitialCommand(theGuild);
        appLogger.info("Guild register ok" + guild.id);
    })


    // handle command interaction
    client.on('interactionCreate', async (interaction) => {
        if (!interaction.isCommand()) {
            return;
        }

        await CommandManager.runCommand(interaction);
    })

    appLogger.info("Logging in to Discord...");
    client.login(process.env.DISCORD_BOT_TOKEN);

}).catch(error => appLogger.error("App encountered an error", error));
