import {MigrationInterface, QueryRunner} from "typeorm";

export class AddIndexForTables1638675637999 implements MigrationInterface {
    name = 'AddIndexForTables1638675637999'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE UNIQUE INDEX \`IDX_0fe10bd094379e772795fea2a3\` ON \`command_registry\` (\`commandId\`, \`serverId\`)`);
        await queryRunner.query(`CREATE UNIQUE INDEX \`IDX_8d067a43ad04bf798ca358358b\` ON \`poke_request\` (\`guildId\`, \`channelId\`, \`pingTo\`, \`pingType\`)`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP INDEX \`IDX_8d067a43ad04bf798ca358358b\` ON \`poke_request\``);
        await queryRunner.query(`DROP INDEX \`IDX_0fe10bd094379e772795fea2a3\` ON \`command_registry\``);
    }

}
