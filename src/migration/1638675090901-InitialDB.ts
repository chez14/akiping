import {MigrationInterface, QueryRunner} from "typeorm";

export class InitialDB1638675090901 implements MigrationInterface {
    name = 'InitialDB1638675090901'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE \`guild_registry\` (\`id\` int NOT NULL AUTO_INCREMENT, \`guildId\` varchar(255) NOT NULL, \`isAllowed\` tinyint NOT NULL, \`allowedBy\` varchar(255) NOT NULL, \`createdOn\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), \`updatedOn\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6), \`deletedOn\` datetime(6) NULL, \`commandsId\` int NULL, UNIQUE INDEX \`IDX_1dfa4a99a4002194922e7ea8ed\` (\`guildId\`), PRIMARY KEY (\`id\`)) ENGINE=InnoDB`);
        await queryRunner.query(`CREATE TABLE \`command_registry\` (\`id\` int NOT NULL AUTO_INCREMENT, \`commandId\` varchar(255) NOT NULL, \`commandName\` varchar(255) NOT NULL, \`version\` int NOT NULL, \`createdOn\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), \`updatedOn\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6), \`deletedOn\` datetime(6) NULL, \`serverId\` int NULL, PRIMARY KEY (\`id\`)) ENGINE=InnoDB`);
        await queryRunner.query(`CREATE TABLE \`poke_request\` (\`id\` int NOT NULL AUTO_INCREMENT, \`guildId\` varchar(255) NOT NULL, \`channelId\` varchar(255) NOT NULL, \`pingTo\` varchar(255) NOT NULL, \`pingType\` enum ('1', '2') NOT NULL, \`hasNewMessage\` tinyint NOT NULL DEFAULT 0, \`createdOn\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), \`deletedOn\` datetime(6) NULL, PRIMARY KEY (\`id\`)) ENGINE=InnoDB`);
        await queryRunner.query(`CREATE TABLE \`bind_request\` (\`id\` int NOT NULL AUTO_INCREMENT, \`guildId\` varchar(255) NOT NULL, \`channelId\` varchar(255) NOT NULL, \`pingTo\` varchar(255) NOT NULL, \`hasNewMessage\` tinyint NOT NULL, \`createdOn\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), \`deletedOn\` datetime(6) NULL, PRIMARY KEY (\`id\`)) ENGINE=InnoDB`);
        await queryRunner.query(`ALTER TABLE \`guild_registry\` DROP INDEX \`IDX_1dfa4a99a4002194922e7ea8ed\``);
        await queryRunner.query(`CREATE UNIQUE INDEX \`IDX_1dfa4a99a4002194922e7ea8ed\` ON \`guild_registry\` (\`guildId\`)`);
        await queryRunner.query(`ALTER TABLE \`guild_registry\` ADD CONSTRAINT \`FK_0591db5ff9374d7c231d0afd481\` FOREIGN KEY (\`commandsId\`) REFERENCES \`command_registry\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE \`command_registry\` ADD CONSTRAINT \`FK_054beaf978c590505066730eed3\` FOREIGN KEY (\`serverId\`) REFERENCES \`guild_registry\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`command_registry\` DROP FOREIGN KEY \`FK_054beaf978c590505066730eed3\``);
        await queryRunner.query(`ALTER TABLE \`guild_registry\` DROP FOREIGN KEY \`FK_0591db5ff9374d7c231d0afd481\``);
        await queryRunner.query(`DROP INDEX \`IDX_1dfa4a99a4002194922e7ea8ed\` ON \`guild_registry\``);
        await queryRunner.query(`ALTER TABLE \`guild_registry\` ADD UNIQUE INDEX \`IDX_1dfa4a99a4002194922e7ea8ed\` (\`guildId\`)`);
        await queryRunner.query(`DROP TABLE \`bind_request\``);
        await queryRunner.query(`DROP TABLE \`poke_request\``);
        await queryRunner.query(`DROP TABLE \`command_registry\``);
        await queryRunner.query(`DROP INDEX \`IDX_1dfa4a99a4002194922e7ea8ed\` ON \`guild_registry\``);
        await queryRunner.query(`DROP TABLE \`guild_registry\``);
    }

}
