import {MigrationInterface, QueryRunner} from "typeorm";

export class RemoveBinderRequestTable1638689089022 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE \`bind_request\``);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
    }

}
