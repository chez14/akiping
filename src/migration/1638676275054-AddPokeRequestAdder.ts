import {MigrationInterface, QueryRunner} from "typeorm";

export class AddPokeRequestAdder1638676275054 implements MigrationInterface {
    name = 'AddPokeRequestAdder1638676275054'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`poke_request\` ADD \`addedBy\` varchar(255) NOT NULL`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`poke_request\` DROP COLUMN \`addedBy\``);
    }

}
