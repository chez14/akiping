import {MigrationInterface, QueryRunner} from "typeorm";

export class AddDefaultValueForGuildRegistry1638683543062 implements MigrationInterface {
    name = 'AddDefaultValueForGuildRegistry1638683543062'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`guild_registry\` CHANGE \`isAllowed\` \`isAllowed\` tinyint NOT NULL DEFAULT 0`);
        await queryRunner.query(`ALTER TABLE \`guild_registry\` CHANGE \`allowedBy\` \`allowedBy\` varchar(255) NULL`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`guild_registry\` CHANGE \`allowedBy\` \`allowedBy\` varchar(255) NOT NULL`);
        await queryRunner.query(`ALTER TABLE \`guild_registry\` CHANGE \`isAllowed\` \`isAllowed\` tinyint NOT NULL`);
    }

}
