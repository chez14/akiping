import { MessageEmbed } from "discord.js";

export default function (error: Error | ReferenceError): MessageEmbed {
    let msg = new MessageEmbed();
    msg.setColor("#ff0000");
    msg.setAuthor("Debugger");
    msg.setTitle("Uncatched Exception")
    msg.setDescription("`" + error.constructor.name + "`: " + error.message)

    if(process.env.NODE_ENV === "development") {
        msg.addField("Stack", ("```\n" + (error.stack?.substr(0, 500)) + "\n```"), false)
    }

    return msg;
}
