import { REST } from "@discordjs/rest";


export const discordRestVersion: string = "9";

export function getDiscordRestClient(): REST {
    return new REST({ version: discordRestVersion }).setToken(process.env.DISCORD_BOT_TOKEN || "");
}